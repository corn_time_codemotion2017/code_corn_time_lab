import React from 'react';
import CSSModules from 'react-css-modules';
import FaHeart from 'react-icons/lib/fa/heart';
import FaEye from 'react-icons/lib/fa/eye';
import {connect} from 'react-redux';
// import {FaHeart, FaEye} from 'react-icons/lib/fa';
import {setBookmarkActionCreator, setSeenActionCreator, setFocusActionCreator} from '../action_reducer';
import styles from './poster.css';

function PosterComponent({
                           id,
                           title,
                           year,
                           seen=false,
                           bookmark=false,
                           userFocused=false,
                           posterSrc = '',
                           posterClickAction = function (id) {
                             console.log('poster click', id);
                           },
                           seenClickAction = function (id) {
                             console.log('seen click', id);
                           },
                           bookmarkClickAction = function (id) {
                             console.log('bookmark click', id);
                           },
                           focusAction = function(id){
                             console.log('focus action', id);
                           }
                         }) {

  const posterStyle = (!posterSrc) ? null : {backgroundImage: `url(${posterSrc})`};
  const posterWrapperStyle = (!userFocused)?'poster-wrapper':'poster-wrapper-focus';
  const posterStyleName = (userFocused||seen||bookmark)?'poster-used':'poster';
  const seenStyleName = (seen)?'seen':'icon';
  const bookmarkStyleName = (bookmark)?'bookmarked':'icon';

  return (
    <div styleName="wrapper" onClick={(event) => {
      posterClickAction(id)
    }} onMouseEnter={()=>focusAction({id, userFocused:true})} onMouseLeave={()=>focusAction({id, userFocused:false})} >
      <div styleName={posterWrapperStyle}>
        <div styleName="actions">
          <FaEye styleName={seenStyleName} onClick={(event) => {
            event.stopPropagation();
            seenClickAction({id, seen:!seen})
          }}/>
          <FaHeart styleName={bookmarkStyleName} onClick={(event) => {
            event.stopPropagation();
            bookmarkClickAction({id, bookmark:!bookmark})
          }}/>
        </div>
      </div>
      <div styleName={posterStyleName} style={posterStyle}/>
      <div styleName="text">
        <div>{title}</div>
        <div styleName="year">{year}</div>
      </div>
    </div>
  );
}


export default CSSModules(PosterComponent, styles);

