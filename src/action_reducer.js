import fetch from 'isomorphic-fetch';
const SET_BOOKMARK = 'SET_BOOKMARK';
const SET_SEEN = 'SET_SEEN';
const SET_FOCUS = 'SET_FOCUS';
const SET_CONTENT = 'SET_CONTENT';

const initialState = {
  contents: []
}

export function setBookmarkActionCreator({bookmark, id}){
  return {type: SET_BOOKMARK, bookmark, id}
}

export function setSeenActionCreator({seen, id}){
  return {type: SET_SEEN, seen, id}
}

export function setFocusActionCreator({userFocused, id}){
  return {type: SET_FOCUS, userFocused, id}
}

export function setContents(contents){
  return {type: SET_CONTENT, contents};
}

export function fetchContentsAction(dispatch){
  return (dispatch)=>{
    return fetch(`http://franciscomar.com.es/api/movies/1?genre=&sorting=&order-1&keywords=`).then((res)=> {
      return res.json();
    }).then((data)=>{
      console.log('received data', data);
      dispatch(setContents(data))
    });
  }
}

export default function reducer(state, action){
  const {type, id} = action;
  switch(type)  {
    case SET_CONTENT:
      state = Object.assign({}, {contents: action.contents});
    case SET_FOCUS:
      const {userFocused} = action;
      const contents = state.contents.map(
        (props)=>{
          const t = (props._id==id)?Object.assign({},props,{userFocused}):props
          return t;
        }
      ).slice()
      state = Object.assign({}, {contents: contents.slice()});
      break;
    case SET_BOOKMARK:
      const {bookmark} = action;
      const contentsBookmark = state.contents.map(
        (props)=>{return(props._id==id)?Object.assign({},props,{bookmark}):props}
      ).slice()
      state = Object.assign({}, {contents: contentsBookmark.slice()});
      break;
    case SET_SEEN:
      const {seen} = action;
      const contentsSeen = state.contents.map(
        (props)=>{return(props._id==id)?Object.assign({},props,{seen}):props}
      ).slice()
      state = Object.assign({}, {contents: contentsSeen.slice()});
      break;
  }
  return state;
}




