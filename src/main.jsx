import React from 'react';
import PosterComponent from './components/PosterComponent';
import {Provider} from 'react-redux';
import {setBookmarkActionCreator, setSeenActionCreator, setFocusActionCreator, fetchContentsAction} from './action_reducer'
import posterMock from './components/poster.mock.json';
import {connect} from 'react-redux';

const container = (containerConfig)=>{
  return (
    <div>
      <button onClick={()=>containerConfig.fetchContentsAction()}>fetch</button>
      <div>
      {
      containerConfig.contents.map((config)=>{
        const {poster} = config.images;
        const {_id} = config;
        return Object.assign({}, config, {posterSrc: poster, id: _id});
      }).map((config)=>{
        return <PosterComponent key={config.id} {...Object.assign(config, posterDispatchConfig(containerConfig))} />
      })
      }</div></div>
  )
}

const mapStateToProps = ({contents})=>{
  return {
    contents
  };
};

const mapDispatchToProps = (dispatch)=>{
  return {
    fetchContentsAction: ()=>{
      dispatch(fetchContentsAction())
    },
    focusAction:({id, userFocused})=>{
      dispatch(setFocusActionCreator({id, userFocused}))
    },
    seenClickAction:({id, seen})=>{
      dispatch(setSeenActionCreator({id, seen}))
    },
    bookmarkClickAction:({id, bookmark})=>{
      dispatch(setBookmarkActionCreator({id, bookmark}))
    }
  };
}

const posterDispatchConfig = ({focusAction, seenClickAction, bookmarkClickAction})=>{
  return {
    focusAction:({id, userFocused})=>focusAction({id, userFocused}),
    seenClickAction:({id, seen})=>seenClickAction({id, seen}),
    bookmarkClickAction:({id, bookmark})=>bookmarkClickAction({id, bookmark}),
  }
}

const ConnectedContainer = connect(mapStateToProps, mapDispatchToProps)(container);

export default function Main(store){
  return (
    <div>
      <Provider store={store}>
        <ConnectedContainer/>
      </Provider>
    </div>
  );
}