import React from 'react';
import ReactDOM from 'react-dom';
import Main from './main';
import {createStore} from 'redux';
import reducer from './action_reducer';

import {AppContainer} from 'react-hot-loader';

window.onload = () => {
  const initial = createStore(reducer, window.INITIAL_STATE);
  function render(Component) {
    ReactDOM.hydrate(
        <Component {...initial} />
      , document.getElementById('content'));
  }

  render(Main)
}
