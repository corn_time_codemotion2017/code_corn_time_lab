import React from 'react';
import ReactDOM from 'react-dom';
import reducer from './action_reducer';
import thunkMiddleware from 'redux-thunk';
import {createStore, applyMiddleware, compose} from 'redux'
import posters from '../posters.mock.json';

import Main from './main';

import {AppContainer} from 'react-hot-loader';

window.onload = () => {

  const initialState = {contents: posters};
  const store = createStore(reducer, initialState, compose(applyMiddleware(thunkMiddleware)));

  console.log(store.getState().contents)
  function render(Component) {
    ReactDOM.render(
      <AppContainer>
        <Component {...store} />
      </AppContainer>
      , document.getElementById('content'));
  }

  render(Main)
  if (module.hot) {
    module.hot.accept('./main.jsx', () => {
      const NextMain = require('./main.jsx').default;
      render(NextMain);
    });
  }
}
