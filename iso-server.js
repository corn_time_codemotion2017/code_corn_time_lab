import express from 'express';
import React from 'react';
import {renderToString} from 'react-dom/server';
import reducer from './src/action_reducer';
import Main from './src/main.jsx';
import thunkMiddleware from 'redux-thunk';
import  {createStore, applyMiddleware, compose} from 'redux';
import {fetchContentsAction} from './src/action_reducer';
import posters from './posters.mock.json';

const PORT = process.env.PORT || 8090;
const app = express()
app.use(express.static('dist'));
app.get('/',(req, res)=>{
  const initialState = {contents: posters};
  const store  = createStore(reducer, initialState, compose(applyMiddleware(thunkMiddleware)));
  store.dispatch(fetchContentsAction());
  const content =  renderToString(<Main {...store} />);
  const html = `
    <html lang="en">
      <head>
        <link href="./main.css" rel="stylesheet"/>
      </head>
      <body>
      <div id="content">
        ${content} 
      </div>
      </body>
      <script src="./meta.js"></script>
      <script src="./vendor.js"></script>
      <script src="./main.js"></script>
      <script>
        window.INITIAL_STATE=${JSON.stringify(initialState)};
      </script>
    </html>
  `
  res.send(html);
});
app.listen(PORT, ()=>{
  console.log(`listening on port ${PORT}`);
});
